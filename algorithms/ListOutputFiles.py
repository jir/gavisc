
def FilesOut_gavisc(format_in,job):
#===================================================================================
# Input: 

# array "format_in" as [a,b], where
# a and b correspond to VOTable and csv output respectively with 1=output and 0=no output.

# Job ID "job"
#==================================================================================

	path="/data/output/"+job # path to job output 

# File name array generation for A-IS
	base=["lda","probabilities","residual","spine_fit","spectrum"]	
	filelist_txt=[]
	for i in range(0,len(base)):
		filelist_txt.append(base[i]+".txt") # .txt output by default
	if format_in[0]==1: # if VOTable output was selected
		filelist_xml=[]
		for i in range(0,len(base)):
			filelist_xml.append(base[i]+".xml")
	if format_in[1]==1: # If csv output was selected
		filelist_csv=[]
		for i in range(0,len(base)):
			filelist_csv.append(base[i]+".csv")

	filelist_ais=filelist_txt+filelist_xml+filelist_csv
