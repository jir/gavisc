# Inits for testing the AGAVE SC algorithm

import os
import sys
import numpy as np

t = os.path.dirname(os.path.realpath(__file__))
os.chdir(t)

import avisclib as asc
import testdata
# Testing classification function with example spectra
print(dir())
print ""
print "Testing GAVISC BusDeMeo classification..."

for i in range(len(testdata.exampleSpectraLabels)):
  print "---"
  t = asc.GaiaBusDeMeo(testdata.exampleSpectra[i])
  print "Classifying spectra from "+testdata.exampleSpectraLabels[i]+" class"
  print "Classified to "+t.taxvec[0]+" with probability "+ \
        str(round(100*t.probvec[0],4))+" %"

print ""
print "Done"
