

from avi.utilities import path, is_file, get_size, load, glob
from avi.settings import OUTPUT_PATH, SHARE_PATH, METADATA_PATH
from json import loads

from logging import getLogger
logger = getLogger(__name__)

class Results(object):

    def __init__(self, md5, shared=False):
        self.md5 = md5
        self.shared = shared
        self.base = OUTPUT_PATH
        if shared:
            self.base = SHARE_PATH

    def _path(self, *segments):
        return path(self.base, self.md5, *segments)

    def retrieve_metadata(self):
        metadata = loads(load(self._path(METADATA_PATH)))
        return metadata

    def retrieve_viz2d(self):
        viz2d = 'No plot available.'
        try:
            viz2d = load(path(OUTPUT_PATH, self.md5, 'Output/plot.html'))
        except:
            pass
        return viz2d 

logger.info('{} loaded'.format(__name__))

