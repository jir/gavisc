"""
Various default values
"""

from logging import getLogger
logger = getLogger(__name__)

# factory defaults for algorithm parameters
# different algorithms have different defaults 

SEARCH_DEFAULTS = {
    #'search': True,
    'astno': '0',
    'astname': 'None',
    'astdsg': '0000XX0',
}

OUTPUT_DEFAULTS = {
    'csv': True,
    'vot': True,
    'email': True,
}


# collected factory defaults
ALL_DEFAULTS = {}
ALL_DEFAULTS.update(SEARCH_DEFAULTS)
ALL_DEFAULTS.update(OUTPUT_DEFAULTS)



# ordering for listing job states
JOB_STATES_COLLATION = {
    'PENDING': 0,
    'STARTED': 10,
    'SUCCESS': 20,
    'FAILURE': 30,
}



# various query defaults
DEFAULT_SEARCH_QUERY = ''
DEFAULT_SEARCH_ORDER_BY = 'astno'
DEFAULT_SEARCH_ORDER = 'ascending'

DEFAULT_JOBS_QUERY = 'own'
DEFAULT_JOBS_ORDER_BY = 'status'
DEFAULT_JOBS_ORDER = 'ascending'

DEFAULT_PAGE_NO = 1
DEFAULT_PAGE_SIZE = 50





logger.info('{} loaded'.format(__name__))
