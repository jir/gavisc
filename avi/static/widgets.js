function Widgets() {

    'use strict';

    function updateDOM(updates) {
        forEnumerableOwn(updates, function (selector, update) {
            var node = document.querySelector(selector);
            forEnumerableOwn(update, function (value, key) {
                var templateID, template;
                if (key === '_content') {
                    templateID = node.getAttribute('data-template-id');
                    if (templateID != null) {
                        template = document.getElementById(templateID).textContent;
                        node.innerHTML = renderHTML(template, value);
                    } else {
                        node.textContent = value;
                    }
                } else {
                    node.setAttribute(name, value);
                }
            });
        });
    }



    function onSearchInput(node) {
        var query = normalizeWhitespace(node.value);
        var target = document.querySelector(node.getAttribute('data-target'));
    }


    function onPagingClick(node) {
        var target = document.querySelector(node.getAttribute('data-target'));
    }





    function onColumnHeaderClick(node) {
        var shallToggleOrder = node.classList.contains('selected');
        forEach.call(node.parentNode.getElementsByClassName('selected'), function (node) {
            node.classList.remove('selected');
        });
        if (shallToggleOrder) {
            if (node.classList.contains('descending')) {
                node.classList.remove('descending');
            } else {
                node.classList.add('descending');
            } 
        }
        node.classList.add('selected');
    }

    function onRowClick(node) {
        location.replace(node.getAttribute('data-href'));
    }

    function onToggleModalContent(node) {}









    // non-generics

    function onToggleColumn(node) {}

    function onValidateParameters(node) {}







    return {
        onSearchInput: onSearchInput,
        onPagingClick: onPagingClick,
        onColumnHeaderClick: onColumnHeaderClick,
        onRowClick: onRowClick,
        
        onToggleModalContent: onToggleModalContent,
        onToggleColumn: onToggleColumn
    };
}



