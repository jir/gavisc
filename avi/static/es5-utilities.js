

function ES5Utilities() {

    'use strict';

    var toString = Object.prototype.toString;

    /*
    function is(val, spec) {
        return toString.call(val) === '[object ' + spec + ']'; 
    }
    */

    function options(a, b) {
        var i, c;
        if (a != null) return a;
        if (b != null) return b;
        for (i = 2; i < arguments.length; i++) {
            c = arguments[i];
            if (c != null) return c;
        }
        throw TypeError('All options nil');
    }

    function peek(arr, i) {
        i = options(i, -1);
        if (i < 0) {
            i = arr.length + i;
        }
        return arr[i];
    }

    function arrayify() {
        var array = new Array(arguments.length);
        var i;
        for (i = 0; i < arguments.length; i++) {
            array[i] = arguments[i];
        }
        return array;
    }
    
    function hasEnumerableOwn(obj, key) {
        if (obj == null) return false; // TODO keep or remove?
        if (!obj.hasOwnProperty(key)) return false;
        if (!obj.propertyIsEnumerable(key)) return false;
        return true;
    }

    function getEnumerableOwn(obj, key) {
        if (hasEnumerableOwn(obj, key)) return obj[key];
        return void 0;
    }

    function forEnumerableOwn(obj, callback) {
        Object.keys(obj).forEach(function (key) {
            callback(obj[key], key, obj);
        });
    }

    /*
    function popEnumerableOwn(obj, key) {
        var val;
        if (hasEnumerableOwn(obj, key)) {
            val = obj[key];
            if (delete obj[key]) throw TypeError('Unable to pop'); // TODO throw or return undefined?
            return val;
        }
        return void 0;
    }

    function mapEnumerableOwn(obj, callback) {
        var mapped = {};
        forEnumerableOwn(obj, function (val, key) {
            mapped[key] = callback(obj[key], key, obj);
        });
        return mapped;
    }

    function filterEnumerableOwn(obj, callback) {
        var filtered = {};
        forEnumerableOwn(obj, function (val, key) {
            if (!callback(obj[key], key, obj)) return;
            mapped[key] = obj[key];
        });
        return filtered;
    }
    */

    /*
    function assign(dst) { // TODO name?
        var i, src;
        dst = Object(dst); // TODO keep or remove?
        for (i = 1; i < arguments.length; i++) {
            src = arguments[i];
            forEnumerableOwn(src, function (val, key) {
                dst[key] = src[key];
            });
        }
        return dst; // TODO keep or remove?
    }
    */

    function reassign(dst) { // TODO name?
        var i, src;
        dst = Object(dst); // TODO keep or remove?
        for (i = 1; i < arguments.length; i++) {
            src = arguments[i];
            forEnumerableOwn(dst, function (val, key) {
                if (src.hasOwnProperty(key)) { // TODO may fail inconveniently?
                    dst[key] = src[key];
                }
            });
        }
        return dst; // TODO keep or remove?
    }

    /*
    function whitelistEnumerableOwn(src, whitelist) {
        var whitelisted = {};
        forEnumerableOwn(whitelist, function (val, key) {
            whitelisted[key] = src[key];
        });
    }

    function blacklistEnumerableOwn(src, blacklist) {
        var notBlacklisted = {};
        forEnumerableOwn(src, function (val, key) {
            if (!hasOwnProperty(blacklist, key)) {
                whitelisted[key] = src[key];
            }
        });
    }
    */

    // strip leading, trailing and multiple whitespace characters
    function normalizeWhitespace(s) {
        return s.replace(/[\s\uFEFF\xA0]+/g, ' ').trim();
    }

    // naive string formatter
    function format(spec) {
        var args = arrayify.apply(null, arguments);
        var i = 1;
        return spec.replace(/%[snjb%]/g, function (m) {
            if (i >= args.length) return '';
            if (m === '%s') return String(args[i++]);
            if (m === '%n') return Number(args[i++]);
            if (m === '%j') return JSON.stringify(args[i++]);
            if (m === '%b') return Boolean(args[i++]);
            if (m === '%%') return '%';
            throw SyntaxError(format('Bad specifier: %s', m));
        });
    }

    function once(fn) {
        var called = false;
        return function _once() {
            if (!called) {
                called = true;
                return fn.apply(this, arguments);
            }
        };
    }
    function throttle(fn, t) {
        var throttling = false;
        return function _throttled() {
            if (!throttling) {
                throttling = true;
                setTimeout(function _unthrottle() {
                    throttling = false;
                }, t);
                return fn.apply(this, arguments);
            }
        };
    }

    function debounce(fn, t) {
        var timeoutID = 0;
        return function _debounced() {
            var args = arrayify.apply(null, arguments);
            clearTimeout(timeoutID);
            timeoutID = setTimeout(function _bounce() {
                timeoutID = 0;
                return fn.apply(this, args);
            }, t);
        };
    }

    function identity(a) {
        return a;
    }

    function dummy() {}
    
    /*
    function Tuple() {
        var i;
        for (i = 0; i < arguments.length; i++) {
            this[i] = arguments[i];
        }
        this.length = i; // TODO remove?
    }
    
    function createTuple() {
        return Object.seal(arrayify.apply(null, arguments));
    }
    
    */
    
    function NotImplementedError() {
        Error.apply(this, arguments);
    }
    
    NotImplementedError.prototype = Object.create(Error.prototype);
    NotImplementedError.prototype.constructor = NotImplementedError;

    return {
    
        toString: toString,
    
        options: options,
        
        peek: peek,
        arrayify: arrayify,
        
        hasEnumerableOwn: hasEnumerableOwn,
        getEnumerableOwn: getEnumerableOwn,
        forEnumerableOwn: forEnumerableOwn,
        reassign: reassign,
        
        normalizeWhitespace: normalizeWhitespace,
        format: format,
        
        once: once,
        throttle: throttle,
        debounce: debounce,
        identity: identity,
        dummy: dummy,
        
        NotImplementedError: NotImplementedError
    };
}


