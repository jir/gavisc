"""
HTTP request handling
"""

from logging import getLogger
logger = getLogger(__name__)

from hashlib import md5
from datetime import datetime
from json import loads, dumps

from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.shortcuts import render

from django.http import HttpResponse
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.status import HTTP_403_FORBIDDEN
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from pipeline.models import AviJobRequest, PipeState

from avi.utilities import suffix
from avi.utilities import rmdir
from avi.utilities import mkdir
from avi.utilities import save
from avi.utilities import load
from avi.utilities import glob
from avi.utilities import path
from avi.utilities import update_existing
from avi.utilities import get_dir_size
from avi.utilities import is_dir

from avi.data_source import search
from avi.models import Job
from avi.settings import OUTPUT_PATH, SHARE_PATH, METADATA_PATH

from avi.defaults import DEFAULT_SEARCH_QUERY
from avi.defaults import DEFAULT_SEARCH_ORDER_BY
from avi.defaults import DEFAULT_SEARCH_ORDER

from avi.defaults import DEFAULT_JOBS_QUERY
from avi.defaults import DEFAULT_JOBS_ORDER_BY
from avi.defaults import DEFAULT_JOBS_ORDER

from avi.defaults import DEFAULT_PAGE_NO
from avi.defaults import DEFAULT_PAGE_SIZE

from avi.defaults import ALL_DEFAULTS

from avi.settings import VERSION

from avi.results import Results

from plugins.util import generate_userspace_view_link
from plugins.util import generate_userspace_download_link
from plugins.util import generate_dataproduct_share_link

### helpers

def _resolve_results(md5):
    if is_dir(path(OUTPUT_PATH, md5)):
        return Results(md5)
    if is_dir(path(SHARE_PATH, md5)):
        return Results(md5, shared=True)
    raise Http404('Job not found {}'.format(md5))

def _normalize(raw_parameters):
    """
    Normalize computation job parameters for validation and job creation
    """
    parameters = ALL_DEFAULTS.copy()
    update_existing(parameters, raw_parameters)
    return parameters

def _job_cmp(order_by, order):
    def _ascending(a, b):
        return cmp(a[order_by], b[order_by])
    def _descending(a, b):
        return -1 * cmp(a[order_by], b[order_by])
    if order == 'descending':
        return _descending
    return _ascending
    

def _job_paging(query, order_by, order, page_no, page_size):
    """
    Extract a result page and paging info from a job queryset
    """
    # get queryset
    queryset = Job.objects.all()
    queryset = map(_job_mapper, queryset)
    queryset.sort(_job_cmp(order_by, order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 1)
    page = queryset[(page_no - 1) * page_size:page_no * page_size]
    return page, page_no, pages_total

def _job_mapper(job):
    """
    Convert job list item to a format
    accepted by the HMI.
    """
    pipe_state = job.request.pipeline_state
    context = {}
    context.update({
        'md5': job.md5,
        'timestamp': job.request.created.strftime('%Y-%m-%d %H:%M'),
        'astno': job.astno,
        'astname': job.astname,
        'astdsg': job.astdsg,
        'progress': int(pipe_state.get_percentage_complete()),
        'status': pipe_state.state,
    })
    return context

def _shared_job_mapper(metadata):
    """
    Convert job list item to a format
    accepted by the HMI

    Essentially just filter properties.
    """
    context = {}
    context.update({
        'md5': metadata['md5'],
        'timestamp': metadata['timestamp'],
        'astno': metadata['astno'],
        'astname': metadata['astname'],
        'astdsg': metadata['astdsg'],
        'specc': metadata['specc'],
        'specocf': metadata['specocf'],
    })
    return context

def _shared_job_paging(query, order_by, order, page_no, page_size):
    """
    Extract a result page and paging info from a job queryset
    """

    # get queryset
    queryset = map(loads, map(load, glob(path(SHARE_PATH, '*', METADATA_PATH))))
    queryset = map(_shared_job_mapper, queryset)
    queryset.sort(_job_cmp(order_by, order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 1)
    page = queryset[(page_no - 1) * page_size:page_no * page_size]
    return page, page_no, pages_total

### handlers

@require_http_methods(['GET'])
def index(request):
    context = {'version': VERSION}
    return render(request, 'avi/introduction.html', context=context)

@require_http_methods(['GET'])
def searchAsteroids(request):
    return render(request, 'avi/search-asteroids.html')

@require_http_methods(['GET'])
def computationParameters(request):
    context = ALL_DEFAULTS.copy()
    update_existing(context, request.GET)
    return render(request, 'avi/computation-parameters.html', context=context)

@require_http_methods(['GET'])
def ownComputations(request):
    return render(request, 'avi/own-computations.html')

@require_http_methods(['GET'])
def sharedComputations(request):
    return render(request, 'avi/shared-computations.html')

@require_http_methods(['GET'])
def results(request):
    md5 = request.GET.get('md5', '')
    context = ALL_DEFAULTS.copy()
    context['md5'] = md5
    update_existing(context, loads(get_object_or_404(Job, md5=md5).json))
    return render(request, 'avi/results.html', context=context)

@require_http_methods(['GET'])
def viz2d(request, md5):
    viz2d = get_object_or_404(Job, md5=md5).get_viz2d()
    return HttpResponse(viz2d, content_type='text/html')

class ListSearchResults(APIView):
    def get(self, request):

        query = str(request.query_params.get('query', DEFAULT_SEARCH_QUERY))
        order_by = str(request.query_params.get('order-by', DEFAULT_SEARCH_ORDER_BY))
        order = str(request.query_params.get('order', DEFAULT_SEARCH_ORDER))
        page_no = int(request.query_params.get('page-no', DEFAULT_PAGE_NO))
        page_size = int(request.query_params.get('page-size', DEFAULT_PAGE_SIZE))

        page, page_no, pages_total = search(query, order_by, order, page_no, page_size)

        return Response({
            'query': query,
            'order-by': order_by,
            'order': order,
            'page-no': page_no,
            'pages-total': pages_total,
            'page': page,
        })

class ListOrCreateJobs(APIView):

    def get(self, request):

        query = request.query_params.get('query', DEFAULT_JOBS_QUERY)
        order_by = request.query_params.get('order-by', DEFAULT_JOBS_ORDER_BY)
        order = request.query_params.get('order', DEFAULT_JOBS_ORDER)
        page_no = int(request.query_params.get('page-no', DEFAULT_PAGE_NO))
        page_size = int(request.query_params.get('page-size', DEFAULT_PAGE_SIZE))
        page = None

        if query == 'own-computations':
            page, page_no, pages_total = _job_paging(query, order_by, order, page_no, page_size)
        if query == 'shared-computations':
            page, page_no, pages_total = _shared_job_paging(query, order_by, order, page_no, page_size)
        if page == None:
            return Response(status=HTTP_400_BAD_REQUEST)

        return Response({
            'query': query,
            'order-by': order_by,
            'order': order,
            'page-no': page_no,
            'pages-total': pages_total,
            'page': page,
        })

        return Response(status=HTTP_400_BAD_REQUEST) # default return

    def post(self, request):

        parameters = _normalize(request.POST)
        json = dumps(parameters)
        hash = md5()
        hash.update(datetime.utcnow().isoformat())
        hash.update(json)
        parameters['json'] = json
        parameters['md5'] = hash.hexdigest()

        # create and setup job directory
        mkdir(path(OUTPUT_PATH, parameters['md5']))
        save(path(OUTPUT_PATH, parameters['md5'], METADATA_PATH), data=dumps(parameters))

        # create instance
        Job.objects.create(**parameters);
        logger.info('Created a new job: {}'.format(parameters['md5']))
        return Response(status=HTTP_201_CREATED)

class RetrieveOrDestroyJob(APIView):

    def get(self, request, md5):
        job_info = loads(get_object_or_404(Job, md5=md5).json)
        logger.info('Retrieved job info for {}'.format(md5))
        return Response(_wrap(job_info))

    def delete(self, request, md5):
        get_object_or_404(Job, md5=md5).delete()
        if is_dir(path(OUTPUT_PATH, md5)):
            rmdir(path(OUTPUT_PATH, md5), True)
        logger.info('Deleted job {}'.format(md5))
        return Response(status=HTTP_204_NO_CONTENT)

class RetrieveJobProgress(APIView):
    def get(self, request, md5):
        job = get_object_or_404(Job, md5=md5)
        pipe_state = job.request.pipeline_state
        progress = int(pipe_state.get_percentage_complete())
        status = pipe_state.state
        is_completed = status == 'SUCCESS' or status == 'FAILURE'
        logger.info('Retrieved progress for job {}'.format(md5))
        return Response({
            'progress': progress,
            'is-completed': is_completed,
        })
'''#
class RetrieveArchive(APIView):
    def get(self, request, md5):
        archive = get_object_or_404(Job, md5=md5).get_archive()
        logger.info('Retrieved archive for job {}'.format(md5))
        response = Response(archive, content_type='application/x-gzip')
        response['Content-Disposition'] = 'attachment; filename="archive.tgz"'
        return response
#'''

# portal interactions

@require_http_methods(['GET'])
def share(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    description = 'GAVISC results'
    tags = 'gavisc' # TODO add tags
    size = get_dir_size(file_path)
    link = generate_dataproduct_share_link(file_path, description, tags, size)
    return HttpResponse(link, content_type='text/plain')

    
@require_http_methods(['GET'])
def download(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    link = generate_userspace_download_link(file_path)
    return HttpResponse(link, content_type='text/plain')

@require_http_methods(['GET'])
def reveal(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    link = generate_userspace_view_link(file_path)
    return HttpResponse(link, content_type='text/plain')

logger.info('{} loaded'.format(__name__))

