# GAIA AVI SC function library
# Implements algorithm 1 for the Gaia Bus-DeMeo taxonomic classification and
# algorithm 2 for the optical constant fit
#
# 2015, Antti Penttila and Julia Martikainen, Department of Physics, University of Helsinki

import sys
import math
import numpy as np
from scipy.interpolate import RegularGridInterpolator
from scipy.optimize import minimize


###############################################################
# ALGORITHM 1
###############################################################


###############################################################
# PUBLIC FUNCTION


# Compute Bus-DeMeo class probabilities for all asteroid classes
# with given Gaia normalized spectra.
# Called by the AVI GUI.
# Input: Gaia asteroid spectra (numpy float array, length 48)
# Output: BusDeMeoResult-object
def GaiaBusDeMeo(spectra):
  # Drop wavelengths not used by Bus-DeMeo
  bdmspectra = spectra[_drop550]
  # Project spectra to lda-coordinate space
  ldavec = _toLDAcoordinates(bdmspectra)
  # Compute probabilities for the spectra to belong to each B-DM class
  prob = np.zeros(len(_AsteroidClasses))
  for i in range(len(_AsteroidClasses)):
    prob[i] = _singleBusDeMeo(ldavec,_AsteroidClasses[i])
  # Normalize probabilities
  prob /= sum(prob)
  # Round small probabilities to zero
  for i in range(len(_AsteroidClasses)):
    if (prob[i] < 10e-6):
      prob[i] = 0.0

  # Build result object
  BDMresult = BusDeMeoResult(ldavec,prob)

  # Ready
  return BDMresult
########


###############################################################
# PUBLIC CLASS


# Bus-DeMeo classification result class.
# taxvec gives the Bus-DeMeo classes
# probvec gives the Bus-DeMeo class probabilities
# order gives the indices to probvec and taxvec if sorted from
# best-fit (largest probability) to worst-fit (smallest probability)
class BusDeMeoResult:
  def __init__(self,ldacoordinates,probvec):
    self.lda_coordinates = ldacoordinates
    self.order = np.argsort(probvec)[::-1]
    self.taxvec = _taxonomyLabels
    self.probvec = probvec
    self.groupvec = [_taxaGroupLabels[i-1] for i in _taxaGroupIndex]
  ########
########


###############################################################
# INTERNAL FUNCTIONS


# Transform spectra into LDA coordinates
def _toLDAcoordinates(spectra):
  vec = spectra-_LDA.channelMean
  vec = np.dot(vec,_LDA.W)

  return vec
########


# Compute Gaia Bus-DeMeo taxonomic class probability for given
# Gaia normalized spectra
def _singleBusDeMeo(ldavec, asteroidClass):
  vec = ldavec-asteroidClass.ldamean
  prob = asteroidClass.apriori * \
    math.exp(asteroidClass.logcovdet - 0.5*np.dot(np.dot(vec,asteroidClass.inverseCovariance),vec))

  return prob
########


###############################################################
# INTERNAL CLASSES


# Asteroid taxonomic group object
class _AsteroidClass:
  def __init__(self,nameint,apriori,ldamean,covariance,logcovdet,inverseCovariance):
    self.name = _taxonomyLabels[nameint-1]
    self.nameint = nameint
    self.groupint = _taxaGroupIndex[nameint-1]
    self.groupname = _taxaGroupLabels[self.groupint-1]
    self.apriori = apriori
    self.ldamean = ldamean
    self.covariance = covariance
    self.logcovdet = logcovdet
    self.inverseCovariance = inverseCovariance
########


# LDA-classification machine
class _LDAengine:
  def __init__(self,channelMean,W):
    self.channelMean = channelMean
    self.W = W
########


###############################################################
# INTERNAL DATA

_taxonomyLabels = ("S","B","C","X","D","K","L","T","A","O","Q","R","V")
_taxaGroupLabels = ("S   ", "C   ", "X   ", "MISC")
_taxaGroupIndex = (1, 2, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4)

_drop550 = np.array((21, 22, 23, 24, 25, 26, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47), int)

_AsteroidClasses = (
  _AsteroidClass(1, 0.536388,
   np.array([-0.0441577, -0.0517306, 0.0218728], float),
   np.array([[0.00392515, 0.0000524824, -0.000640658], [0.0000524824, 0.00955064, -0.000954727], [-0.000640658, -0.000954727, 0.00335681]], float),
   7.94413,
   np.array([[263.084, 3.67816, 51.2566], [3.67816, 107.82, 31.3677], [51.2566, 31.3677, 316.605]], float)
  ),
  _AsteroidClass(2, 0.0107817,
   np.array([0.0509843, 0.365652, -0.0161046], float),
   np.array([[0.0035525, -0.00421649, 0.000837161], [-0.00421649, 0.00953568, -0.000394621], [0.000837161, -0.000394621, 0.00120583]], float),
   8.5067,
   np.array([[758.22, 317.789, -422.402], [317.789, 239.503, -142.249], [-422.402, -142.249, 1076.01]], float)
  ),
  _AsteroidClass(3, 0.121294,
   np.array([0.13474, 0.264126, -0.0645022], float),
   np.array([[0.00160281, 0.000834978, -0.0000427933], [0.000834978, 0.00478148, 0.0000987735], [-0.0000427933, 0.0000987735, 0.00168536]], float),
   9.08239,
   np.array([[687.351, -120.537, 24.5169], [-120.537, 230.531, -16.5713], [24.5169, -16.5713, 594.938]], float)
  ),
  _AsteroidClass(4, 0.0862534,
   np.array([0.162016, 0.118884, -0.0453303], float),
   np.array([[0.00229263, 0.000132899, -0.000535282], [0.000132899, 0.00650039, -0.000816412], [-0.000535282, -0.000816412, 0.00214225]], float),
   8.62992,
   np.array([[463.379, 5.32296, 117.813], [5.32296, 161.631, 62.9279], [117.813, 62.9279, 520.219]], float)
  ),
  _AsteroidClass(5, 0.0431267,
   np.array([0.317973, -0.13792, -0.10152], float),
   np.array([[0.00541808, -0.0084127, -0.00060824], [-0.0084127, 0.0235692, 0.00129397], [-0.00060824, 0.00129397, 0.00138013]], float),
   7.7757,
   np.array([[416.855, 146.232, 46.61], [146.232, 96.0285, -25.5877], [46.61, -25.5877, 769.102]], float)
  ),
  _AsteroidClass(6, 0.0431267,
   np.array([0.0518608, 0.0909928, 0.051497], float),
   np.array([[0.00281078, -0.000124396, -0.00031259], [-0.000124396, 0.0032892, -0.00015608], [-0.00031259, -0.00015608, 0.00177826]], float),
   8.96176,
   np.array([[363.797, 16.8634, 65.43], [16.8634, 306.078, 29.8293], [65.43, 29.8293, 576.469]], float)
  ),
  _AsteroidClass(7, 0.0592992,
   np.array([0.120945, -0.100189, 0.0430634], float),
   np.array([[0.00364538, 0.00296325, -0.00186009], [0.00296325, 0.0104361, -0.00285982], [-0.00186009, -0.00285982, 0.00362437]], float),
   7.89843,
   np.array([[413.684, -75.6381, 152.627], [-75.6381, 136.086, 68.5605], [152.627, 68.5605, 408.339]], float)
  ),
  _AsteroidClass(8, 0.0107817,
   np.array([0.234461, -0.0118796, -0.0406565], float),
   np.array([[0.00100006, 0.000460477, 0.000236353], [0.000460477, 0.00184122, 0.000210474], [0.000236353, 0.000210474, 0.000940033]], float),
   10.0873,
   np.array([[1179.18, -267.869, -236.505], [-267.869, 618.235, -71.0726], [-236.505, -71.0726, 1139.17]], float)
  ),
  _AsteroidClass(9, 0.0161725,
   np.array([-0.0532355, -0.195513, 0.257492], float),
   np.array([[0.00281437, 0.000885657, -0.00108048], [0.000885657, 0.00140718, -0.000516295], [-0.00108048, -0.000516295, 0.00318298]], float),
   9.09456,
   np.array([[483.966, -259.785, 122.146], [-259.785, 895.06, 56.9978], [122.146, 56.9978, 364.88]], float)
  ),
  _AsteroidClass(10, 0.00269542,
   np.array([-0.354192, 0.385641, 0.0584358], float),
   np.array([[0.000961505, 0.000405856, 0.000310417], [0.000405856, 0.00083355, 0.000359221], [0.000310417, 0.000359221, 0.00086911]], float),
   10.5424,
   np.array([[1354.67, -548.838, -257.], [-548.838, 1682.05, -499.198], [-257., -499.198, 1448.72]], float)
  ),
  _AsteroidClass(11, 0.0215633,
   np.array([-0.188331, 0.162572, 0.0792654], float),
   np.array([[0.00263123, 0.00187594, -0.000752553], [0.00187594, 0.00388066, -0.000526615], [-0.000752553, -0.000526615, 0.0022519]], float),
   8.79402,
   np.array([[620.867, -280.89, 141.798], [-280.89, 393.213, -1.91522], [141.798, -1.91522, 491.01]], float)
  ),
  _AsteroidClass(12, 0.00269542,
   np.array([-0.28523, -0.0763592, 0.0242157], float),
   np.array([[0.00101634, 0.000657742, 0.000414283], [0.000657742, 0.00106816, 0.000488446], [0.000414283, 0.000488446, 0.000954989]], float),
   10.3436,
   np.array([[1684.31, -917.654, -261.32], [-917.654, 1721.95, -482.634], [-261.32, -482.634, 1407.35]], float)
  ),
  _AsteroidClass(13, 0.0458221,
   np.array([-0.53558, -0.175966, -0.197747], float),
   np.array([[0.0271094, 0.0044843, 0.00403447], [0.0044843, 0.0162367, 0.00377369], [0.00403447, 0.00377369, 0.00816225]], float),
   6.2683,
   np.array([[40.5813, -7.33396, -16.6679], [-7.33396, 70.3292, -28.8905], [-16.6679, -28.8905, 144.111]], float)
  )
)

_LDA=_LDAengine(np.array([0.866021, 0.883949, 0.903069, 0.924614, 0.948181, 0.973189, 1.02712, 1.05567, 1.0893, 1.09868, 1.10947, 1.12001, 1.12968, 1.138, 1.14302, 1.14404, 1.13632, 1.12206, 1.10349, 1.08219, 1.06546, 1.05659, 1.06029, 1.07636, 1.10025, 1.12825], float),
  np.array([[-0.0355335, 0.194522, -0.189936], [0.0438771, 0.177012, -0.124145], [0.0285261, 0.238466, -0.120232], [-0.000412435, 0.240931, -0.0941233], [0.0541223, 0.231959, -0.0859277], [0.0765228, 0.210478, -0.010053], [0.0750753, 0.192598, 0.0502989], [0.0153125, 0.145294, 0.0552764], [-0.0667532, -0.0136477, 0.0491933], [-0.0969554, -0.0132496, 0.0552505], [-0.12599, -0.0265554, 0.029503], [-0.123727, -0.0874315, 0.0296439], [-0.209636, -0.132937, 0.0425482], [-0.233758, -0.14665, 0.044154], [-0.283386, -0.0973352, 0.000468629], [-0.215658, -0.17658, -0.0170684], [-0.157924, -0.17673, 0.0436363], [-0.131072, -0.191732, 0.0685299], [-0.0550334, -0.0473083, 0.120393], [0.132489, -0.0497215, 0.211758], [0.275924, 0.105032, 0.306872], [0.43646, 0.168378, 0.299238], [0.470677, 0.142458, 0.20507], [0.333648, 0.0461318, -0.0255314], [0.10456, -0.258264, -0.378985], [-0.194357, -0.608152, -0.681813]], float)
)

###############################################################
# ALGORITHM 2
###############################################################


###############################################################
# PUBLIC FUNCTION


# Find a best-fit material optical constants, as a function of wavelength,
# for the Gaia asteroid spectra.
# Called by the AVI GUI.
# Input: Gaia asteroid spectra (numpy float array, length 48) and
# geometrical albedo (estimate) of the asteroid (float)
# Output: OCFResult-object
def OpticalConstantFit(spectra,taxind):
  # Share minimization data object, needed by the minimized function
  global _mdobj

  # Average albedo from best-fit taxonomy class
  albedo = _average_albedos[taxind]

  # spectra scaled to absolute albedo level
  scspectra = [x*albedo for x in spectra]

  # constraints for minimization and other options
  cons = {'type':'ineq', 'fun':lambda x: _constfun(x)}
  opts = {'rhobeg':-0.001,'maxiter':2500}

  # Run minimization for three initial
  # coefficient vectors each
  res = [i for i in range(5)]
  ibic0 = -3.0*np.ones(_CBS.degree)
  ibic1 = -3.5*np.ones(_CBS.degree)
  ibic2 = -4.0*np.ones(_CBS.degree)
  ibic3 = -4.5*np.ones(_CBS.degree)
  ibic4 = -5.0*np.ones(_CBS.degree)
  # set up data for current minimization function and run minimization with different starting values
  _mdobj = _currentOCFdata(scspectra,_BRdata)
  res[0] = minimize(_OCFminfun,ibic0,method='COBYLA',constraints=cons,options=opts)
  res[1] = minimize(_OCFminfun,ibic1,method='COBYLA',constraints=cons,options=opts)
  res[2] = minimize(_OCFminfun,ibic2,method='COBYLA',constraints=cons,options=opts)
  res[3] = minimize(_OCFminfun,ibic3,method='COBYLA',constraints=cons,options=opts)
  res[4] = minimize(_OCFminfun,ibic4,method='COBYLA',constraints=cons,options=opts)

  # Find best fit
  bi = 0
  brms = res[0].fun

  for i in range(1,5):
    if (res[i].fun < brms):
      bi = i
      brms = res[i].fun

  # Build result object
  bic = res[bi].x
  impowvec = _CBS.CBSarrayvalue(_GWBsc,bic)
  imvec = [pow(10.0,im) for im in impowvec]
  li = np.transpose([_GWB,impowvec])
  fitspectra = _BRdata.brfun(li)
  re = _BRdata.real_part
  result = OCFResult(re,imvec,fitspectra,brms,bi,scspectra,bic,albedo)

  # Ready
  return result
########


###############################################################
# PUBLIC CLASS


# Optical constant fit result class
class OCFResult:
  def __init__(self,re,imvec,spec,rse,bi,orig_spectra,bic,albedo):
    self.fitted_real_part = re
    self.fitted_imaginary_spectra = imvec
    self.fitted_spectra = spec
    self.rse = rse
    self.wavelengths = _GWB
    self.best_case = bi
    self.original_spectra = orig_spectra
    self.spline_coefficients = bic
    self.albedo = albedo
  ########
########


###############################################################
# INTERNAL FUNCTIONS


# Constrain function for minimization
def _constfun(x):

  # constraints are that CBS-modeled imaginary powers
  # are in pre-computed range
  current_impowvec = _CBS.CBSarrayvalue(_GWBsc,x)

  y = 1.0
  for i in range(len(_GWB)):
    if (current_impowvec[i] < _mimpowtab[0]):
      y = current_impowvec[i] - _mimpowtab[0]
      break
    elif (_mimpowtab[-1] < current_impowvec[i]):
      y = _mimpowtab[-1] - current_impowvec[i]
      break

  return y
########


# Objective function to be minimized in the OC fit
def _OCFminfun(x):

  # cubic-spline-modeled im-pow-values
  current_impowvec = _CBS.CBSarrayvalue(_GWBsc,x)

  # modeled spectral values
  li = np.transpose([_GWB,current_impowvec])
  brvec = _mdobj.brfun(li)

  # model-observation rms
  t = brvec-_mdobj.scspectra
  sqerr = np.sqrt(np.dot(t,t))

  return sqerr
########


###############################################################
# INTERNAL CLASSES

# Current data to be used with the OCF minimization function
class _currentOCFdata:
  def __init__(self,scspectra,brobj):
    self.scspectra = scspectra
    self.brfun = brobj.brfun
    self.real_part = brobj.real_part
  ########
########


# Reflectance data object
class _BRdataobj:
  def __init__(self,lamtab,mimpowtab,brtab,re):
    self.lambda_limits = (lamtab[0],lamtab[-1])
    self.m_im_pow_limits = (mimpowtab[0],mimpowtab[-1])
    self.m_im_limits = (math.pow(10, mimpowtab[0]), math.pow(10, mimpowtab[-1]))
    self.brfun = RegularGridInterpolator((lamtab,mimpowtab), brtab)
    self.real_part = re
  ########
########


# GAVISC cubic B-spline fit object
class _CBSengine:

  def __init__(self,coefs,degr):
    self._coef = coefs
    self.degree = degr
  ########

  def CBSarrayvalue(self, xvec, bic):
    yvec = np.zeros(len(xvec))
    found = False
    t = 0.0
    y = 0.0

    # No checks, call only with proper input

    for xi in range(len(xvec)):
      x = xvec[xi]
      y = 0.0
      for i in range(self.degree):
        rtab = self._coef[i]
        found = False
        for rt in rtab:
          if rt[0] <= x and x < rt[1]:
            found = True
            break
        if found:
          t = rt[2] + rt[3]*x + rt[4]*x**2 + rt[5]*x**3
          y = y + bic[i]*t
      yvec[xi] = y

    return yvec
  ########
########


###############################################################
# INTERNAL DATA

# Average albedos for asteroid classes (same order as in
# '_taxonomyLabels' in algorithm 1
_average_albedos = (0.273339,0.0684297,0.0728348,0.0909686,0.171342,0.180267,0.2876,0.250207,0.369875,0.256262,0.0723563,0.351887,0.100307)

# Pre-computed brightness data

_lamtab = np.array(([0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1., 1.1]), float)
_mimpowtab = np.array(([-5.8, -5.2, -4.6, -4., -3.4, -2.8, -2.2]), float)
_brtab = np.array(([[1.03275,0.6681,0.325245,0.071505,0.0124645,0.0113695,0.0111285],
  [1.10625,0.74415,0.39233,0.10881,0.0151595,0.011961,0.01172],
  [1.15175,0.81125,0.44867,0.14268,0.019015,0.011501,0.010953],
  [1.17965,0.85165,0.498045,0.17696,0.0232865,0.0113475,0.0111285],
  [1.20445,0.88735,0.5307,0.206665,0.02861,0.01172,0.0103835],
  [1.2419,0.91265,0.56435,0.235385,0.037197,0.012377,0.011435],
  [1.2557,0.96245,0.60055,0.25622,0.0457845,0.0118075,0.0106685],
  [1.2603,0.971,0.6261,0.274685,0.054875,0.011895,0.011435],
  [1.2799,1.00495,0.6479,0.303495,0.064605,0.0111945,0.0109315]]), float)

_BRdata = _BRdataobj(_lamtab,_mimpowtab,_brtab,1.5)

# Gaia wavelength bands. This is truncated, replace with full range later
_GWB = np.array(([0.33,0.333,0.337,0.34,0.344,0.348,0.352,0.356,0.36,0.365,0.37,0.375,0.38,0.386,0.393,0.399,0.406,0.414,0.422,0.431,0.441,0.452,0.464,0.477,0.492,0.509,0.528,0.55,0.576,0.606,0.643,0.654,0.667,0.681,0.696,0.713,0.73,0.749,0.77,0.793,0.817,0.844,0.873,0.905,0.939,0.977,1.018,1.062]), float)
_GWBsc = [(x-_lamtab[0])/(_lamtab[-1]-_lamtab[0]) for x in _GWB]

# Pre-computed cubic b-spline coefficients (9)
_CBScoef = [[[0, 0.166666666666667, 1.00000000000000, -18.0000000000000, 108.000000000000, -216.000000000000]],
[[0, 0.166666666666667, 0, 18.0000000000000, -162.000000000000, 378.000000000000],
[0.166666666666667, 0.333333333333333, 2.00000000000000, -18.0000000000000, 54.0000000000000, -54.0000000000000]],
[[0, 0.166666666666667, 0, 0, 54.0000000000000, -198.000000000000],
[0.166666666666667, 0.333333333333333, -1.50000000000000, 27.0000000000000, -108.000000000000, 126.000000000000],
[0.333333333333333, 0.500000000000000, 4.50000000000000, -27.0000000000000, 54.0000000000000, -36.0000000000000]],
[[0, 0.166666666666667, 0, 0, 0, 36.0000000000000],
[0.166666666666667, 0.333333333333333, 0.666666666666667, -12.0000000000000, 72.0000000000000, -108.000000000000],
[0.333333333333333, 0.500000000000000, -7.33333333333333, 60.0000000000000, -144.000000000000, 108.000000000000],
[0.500000000000000, 0.666666666666667, 10.6666666666667, -48.0000000000000, 72.0000000000000, -36.0000000000000]],
[[0.166666666666667, 0.333333333333333, -0.166666666666667, 3.00000000000000, -18.0000000000000, 36.0000000000000],
[0.333333333333333, 0.500000000000000, 5.16666666666667, -45.0000000000000, 126.000000000000, -108.000000000000],
[0.500000000000000, 0.666666666666667, -21.8333333333333, 117.000000000000, -198.000000000000, 108.000000000000],
[0.666666666666667, 0.833333333333333, 20.8333333333333, -75.0000000000000, 90.0000000000000, -36.0000000000000]],
[[0.333333333333333, 0.500000000000000, -1.33333333333333, 12.0000000000000, -36.0000000000000, 36.0000000000000],
[0.500000000000000, 0.666666666666667, 16.6666666666667, -96.0000000000000, 180.000000000000, -108.000000000000],
[0.666666666666667, 0.833333333333333, -47.3333333333333, 192.000000000000, -252.000000000000, 108.000000000000],
[0.833333333333333, 1.00000000000000, 36.0000000000000, -108.000000000000, 108.000000000000, -36.0000000000000]],
[[0.500000000000000, 0.666666666666667, -4.50000000000000, 27.0000000000000, -54.0000000000000, 36.0000000000000],
[0.666666666666667, 0.833333333333333, 43.5000000000000, -189.000000000000, 270.000000000000, -126.000000000000],
[0.833333333333333, 1.00000000000000, -144.000000000000, 486.000000000000, -540.000000000000, 198.000000000000]],
[[0.666666666666667, 0.833333333333333, -16.0000000000000, 72.0000000000000, -108.000000000000, 54.0000000000000],
[0.833333333333333, 1.00000000000000, 234.000000000000, -828.000000000000, 972.000000000000, -378.000000000000]],
[[0.833333333333333, 1.00000000000000, -125.000000000000, 450.000000000000, -540.000000000000, 216.000000000000]]
]

_CBS = _CBSengine(_CBScoef,9)
