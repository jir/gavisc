"""
Stub for passing notifications to user

To be implemented during integration phase
"""

from logging import getLogger
logger = getLogger(__name__)


def notify(*args, **kwargs):
    pass



logger.info('{} loaded'.format(__name__))
