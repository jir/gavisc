

"""
GAVISC 2D plots
"""






"""
GAVISC 2D plots
"""
# TODO prune imports
from bokeh.resources import INLINE
from bokeh.plotting import figure
from bokeh.io import hplot, output_file,save
#from bokeh.models import ColumnDataSource,Legend,OpenURL,TapTool,CustomJS
from bokeh.models.tools import HelpTool #, ResetTool
from bokeh.models.widgets import Tabs,Panel,CheckboxGroup,Button
from bokeh.layouts import widgetbox,column,row,gridplot
from bokeh.models import Legend
			 
from numpy import loadtxt, savetxt


def create_plot(output_path):
    """
    GAVISC plotting function
    
    Loads SPEC-C and SPEC-OCF output files, plotting them using Bokeh
    """
    # original matplotlib plots:

    #  plt.subplot(2,1,1)
    #  plt.plot(res2.wavelengths, res2.fitted_imaginary_spectra)
    #  plt.xlabel('wavelength (um)')
    #  plt.ylabel('absorption coefficient')
    #  plt.title('Fitted absorption coefficient')
    #  plt.grid(True)
      
    #  plt.subplot(2,1,2)
    #  plt.plot(res2.wavelengths, res2.fitted_spectra, 'r', res2.wavelengths, res2.original_spectra, 'b')
    #  plt.xlabel('wavelength (um)')
    #  plt.ylabel('geometrical albedo')
    #  plt.title('Fitted (red) and original (blue) spectra')
    #  plt.grid(True)

    spectrum=loadtxt(output_path+"/spectrum.txt",skiprows=1)
    fitted_spectra = spectrum[:,3]
    fitted_imaginary_spectra=spectrum[:,2]
    original_spectra = spectrum[:,1]
    wavelengths = spectrum[:,0]
    with open(output_path+"/probabilities.txt") as file:
        probability = [[str(digit) for digit in line.split()] for line in file]
	taxtypes=range(0,13)
	taxclass=range(0,13)
	probab=range(0,13)
    for i in range(0,13):
        taxtypes[i] = probability[i+1][1]
        taxclass[i] = probability[i+1][2]
        probab[i] = float(probability[i+1][3])
    output_file(output_path+"/plot.html")

    # For some reason "reset" and "save" tools do not both appear
    tools = "save"
    plot1 = figure(title="Fitted absorption coefficient",
                   x_axis_label="Wavelength (um)", y_axis_label="Absorption coefficient",
                   width=480, plot_height=300,
                   tools=tools)
    plot2 = figure(title="Model fit to the original spectrum",
                    x_axis_label='Wavelength (um)', y_axis_label='Spectral brightness',
                    width=480, plot_height=300,
                    tools=tools)
    plot3 = figure(title="Probabilities for taxonomic types",
                    x_axis_label='Taxonomic type', y_axis_label='Probability',
                    x_range=taxtypes,width=480, plot_height=300,
                    tools=tools)

# Link to the user manual output section
    plot1.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="/static/html/manual.html#plots"))
    plot2.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="/static/html/manual.html#plots"))
    plot3.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="/static/html/manual.html#plots"))

    plot1.line(wavelengths, fitted_imaginary_spectra)
    p21=plot2.line(x=wavelengths,y=fitted_spectra,color="red")
    p22=plot2.line(x=wavelengths,y=original_spectra,color="blue")
    legend2 = Legend(legends=[("Original",[p22]),("Fitted",[p21])],location=(0,-30))
    plot2.add_layout(legend2, 'left')
    plot3.circle(x=taxtypes,y=probab)
    p3=plot3.circle_x(x=int(probability[1][0])+1,y=probab[int(probability[1][0])],fill_color="yellow",line_color="indigo")
    glyph3=p3.glyph
    glyph3.size=15
	
    pall=column(row(plot1),row(plot3),row(plot2))    
    save(pall)

    return True

