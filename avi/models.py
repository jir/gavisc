from json import loads
from django.core import serializers

from django.db.models import Model, BooleanField, CharField, TextField, FloatField, BigIntegerField
from pipeline.models import AviJob

from avi.utilities import path, load, glob, is_file, create_tgz
from avi.settings import OWN_JOBS_PATH
from avi.settings import OUTPUT_PATH
from avi.defaults import ALL_DEFAULTS

from logging import getLogger
logger = getLogger(__name__)











class Job(AviJob):

    pipeline_task = 'Compute'

    # job bookkeeping
    md5 = CharField(max_length=100)
    base = CharField(max_length=100)
    json = TextField()

    # data retrieval
    data = BooleanField(default=True)
    astno = CharField(max_length=100)
    astdsg = CharField(max_length=100)
    astname = CharField(max_length=100)

    # format conversions
    csv = BooleanField()
    vot = BooleanField()

    # email notification
    email = BooleanField() # TODO leftover, remove

    def get_parameters(self):
        # TODO returns more than just parameters
        return loads(self.json)

    ''' #
    def get_archive(self):
        # TODO improve!
        create_tgz('/tmp/archive.tgz', path(OWN_JOBS_PATH, self.md5))
        return load('/tmp/archive.tgz', mode='rb')
    # '''

    def get_viz2d(self):
        viz2d = b'No plot available.'
        try:
            viz2d = load(path(OWN_JOBS_PATH, self.md5, 'Output/plot.html'), mode='rb')
        except Exception as e:
            logger.error(repr(e))
        return viz2d 

    def get_path(self):
        return path(OUTPUT_PATH, self.md5)


logger.info('{} loaded'.format(__name__))





