

"""
GAVISC backend computation pipeline

"""

from luigi.event import Event

from pipeline.classes import AviTask, AviLocalTarget, AviParameter

from avi.data_source import request_data
from avi.settings import OUTPUT_PATH, SPECC_BIN, SPECOCF_BIN
from avi.utilities import path, save, mkdir, chdir, save_txt, cmd, glob
from avi.utilities import txt_to_csv, txt_to_vot, splitext
from avi.visualization import create_plot

from logging import getLogger
logger = getLogger(__name__)

## utility classes

class Task(AviTask):
    """
    Boilerplate parent class for pipeline tasks
    
    Automatically manages task working directories,
    marking task marker files, and wrapping dependencies
    with self.task_dependency()
    """
    md5 = AviParameter()
    def _cwd(self):
        return path(OUTPUT_PATH, self.md5)
    def _do(self):
        pass
    def _dependencies(self):
        return None
    def requires(self):
        dependencies = self._dependencies()
        if dependencies:
            if hasattr(dependencies, '__iter__'):
                return [self.task_dependency(d) for d in dependencies]
            return self.task_dependency(dependencies)
        return super(Task, self).requires()
    def run(self):
        self._do()
        save(path(self._cwd(), '.' + self.__class__.__name__))
    def output(self):
        return AviLocalTarget(path(self._cwd(), '.' + self.__class__.__name__))

##

class Compute(Task):
    """ Entry point for jobs """
    def _dependencies(self):
        return SPECC, SPECOCF, Plot, CSV, VOTable

class Plot(Task): # technically superfluous
    def _dependencies(self):
        return PlotHTMLFile

class PlotHTMLFile(Task):
    def _dependencies(self):
        return SPECC, SPECOCF
    def _do(self):
        create_plot(path(self._cwd(), 'Output'))

_DTYPES = {
    'Output/specc_taxvec.txt': str,
    'Output/specc_order.txt': int,
    'Output/specc_groupvec.txt': str,
    'Output/specocf_best_case.txt': int,
}

class CSV(Task):
    def _dependencies(self):
        return SPECC, SPECOCF
    def _do(self):
        txt_paths = glob(path(self._cwd(), 'Output/*.txt'))
        for txt_path in txt_paths:
            prefix, _ = splitext(txt_path)
            try:
                txt_to_csv(txt_path, prefix + '.csv', dtype=_DTYPES.get(txt_path, float))
            except Exception as exc:
                # silently fail, but at least log the event
                logger.error(repr(exc))

class VOTable(Task):
    def _dependencies(self):
        return SPECC, SPECOCF
    def _do(self):
        txt_paths = glob(path(self._cwd(), 'Output/*.txt'))
        txt_paths = glob(path(self._cwd(), 'Output/*.txt'))
        for txt_path in txt_paths:
            prefix, _ = splitext(txt_path)
            try:
                txt_to_vot(txt_path, prefix + '.vot', dtype=_DTYPES.get(txt_path, float))
            except Exception as exc:
                # silently fail, but at least log the event
                logger.error(repr(exc))

## bundle tasks
class SPECC(Task):
    
    def _dependencies(self):
        return (
            SPECCTaxvecTxtFile,
            SPECCProbvecTxtFile,
            SPECCOrderTxtFile,
            SPECCLDACoordinatesTxtFile,
            SPECCGroupvecTxt,
        )

class SPECOCF(Task):
    
    def _dependencies(self):
        return (
            SPECOCFFittedRealPartTxtFile,
            SPECOCFFittedImaginarySpectraTxtFile,
            SPECOCFFittedSpectraTxtFile,
            SPECOCFRseTxtFile,
            SPECOCFWavelengthsTxtFile,
            SPECOCFBestCaseTxtFile,
            SPECOCFOriginalSpectraTxtFile,
            SPECOCFSplineCoefficientsTxtFile,
            SPECOCFAlbedoTxtFile,
        )

## SPEC-C data product wrappers

class SPECCTaxvecTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECC

class SPECCProbvecTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECC

class SPECCOrderTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECC

class SPECCLDACoordinatesTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECC

class SPECCGroupvecTxt(Task):
    def _dependencies(self):
        return ComputeSPECC

## SPEC-OCF data product wrappers

class SPECOCFFittedRealPartTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFFittedImaginarySpectraTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFFittedSpectraTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFRseTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFWavelengthsTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFBestCaseTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFOriginalSpectraTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFSplineCoefficientsTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

class SPECOCFAlbedoTxtFile(Task):
    def _dependencies(self):
        return ComputeSPECOCF

## main algorithm tasks

class ComputeSPECC(Task):
    def _dependencies(self):
        return InputDataTxtFile
    def _do(self):
        cmd(SPECC_BIN, 'Input', 'Output', cwd=self._cwd())

class ComputeSPECOCF(Task):
    def _dependencies(self):
        return InputDataTxtFile, SPECCOrderTxtFile
    def _do(self):
        cmd(SPECOCF_BIN, 'Input', 'Output', cwd=self._cwd())

## initial steps
class InputDataTxtFile(Task):
    def _dependencies(self):
        return RequestInputData # skipping PreprocessInputData

class PreprocessInputData(Task):
    def _dependencies(self):
        return RequestInputData
    def _do(self):
        raise NotImplementedError('Currently no preprocessing defined')

class RequestInputData(Task):
    astno = AviParameter()
    astname = AviParameter()
    astdsg = AviParameter()
    def _dependencies(self):
        return Initialize
    def _do(self):
        spectrum = request_data(self.astno, self.astname, self.astdsg)
        save(path(self._cwd(), 'Input/inputData.txt'), spectrum)

class Initialize(AviTask):
    """ Initial task for jobs, creates a job workspace """
    md5 = AviParameter()
    def _cwd(self):
        return path(OUTPUT_PATH, self.md5)
    def run(self):
        mkdir(path(self._cwd(), 'Input'))
        mkdir(path(self._cwd(), 'Output'))
        save(path(self._cwd(), '.' + self.__class__.__name__))
    def output(self):
        return AviLocalTarget(path(self._cwd(), '.' + self.__class__.__name__))

logger.info('{} loaded'.format(__name__))

