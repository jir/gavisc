"""
Mock data source for GAVITEA
"""

from logging import getLogger
logger = getLogger(__name__)



from random import randint, choice
from string import ascii_lowercase
from avi.settings import USE_MOCK_DATA



def _randdsg(begin, end):
    year = randint(begin, end)
    first_letter = choice(ascii_lowercase)
    second_letter = choice(ascii_lowercase)
    cycle = randint(2, 1000)
    return '{}{}{}{}'.format(year, first_letter, second_letter, cycle)



_MOCK_NAMES = [
    'accumsan', 'adipiscing', 'aenean', 'aliquam',
    'amet', 'ante', 'arcu', 'auctor', 'augue',
    'bibendum', 'blandit', 'commodo', 'condimentum',
    'congue', 'consectetur', 'consequat', 'cras',
    'cubilia', 'curabitur', 'curae', 'cursus',
    'dapibus', 'diam', 'dictum', 'dictumst',
    'dignissim', 'dolor', 'donec', 'efficitur',
    'egestas', 'eget', 'elementum', 'etiam',
    'facilisis', 'faucibus', 'felis', 'fermentum',
    'feugiat', 'finibus', 'fringilla', 'gravida',
    'habitasse', 'hendrerit', 'imperdiet', 'integer',
    'interdum', 'ipsum', 'justo', 'lacus', 'laoreet',
    'lectus', 'leo', 'libero', 'ligula', 'lobortis',
    'lorem', 'luctus', 'magna', 'malesuada', 'massa',
    'mattis', 'mauris', 'maximus', 'metus',
    'molestie', 'mollis', 'morbi', 'neque', 'nibh',
    'nisi', 'non', 'nulla', 'nullam', 'nunc', 'odio',
    'orci', 'ornare', 'pellentesque', 'pharetra',
    'phasellus', 'placerat', 'platea', 'porttitor',
    'posuere', 'praesent', 'pretium', 'primis',
    'proin', 'pulvinar', 'purus', 'quam', 'quis',
    'rhoncus', 'risus', 'rutrum', 'sagittis',
    'sapien', 'scelerisque', 'semper', 'sodales',
    'sollicitudin', 'suspendisse', 'tellus',
    'tempus', 'tincidunt', 'tristique', 'turpis',
    'ullamcorper', 'ultrices', 'urna', 'vehicula',
    'velit', 'venenatis', 'vestibulum', 'vitae',
    'vivamus', 'viverra', 'volutpat', 'vulputate',
]

_MOCK_NUMBERS = [str(randint(10000, 1000000)) for _ in _MOCK_NAMES]
_MOCK_DESIGNATIONS = [str(_randdsg(1990, 2015)) for _ in _MOCK_NAMES]
_MOCK_SEARCH_RESULTS = zip(_MOCK_NUMBERS, _MOCK_NAMES, _MOCK_DESIGNATIONS)








def _matches(prefix, item):
    return item[0].startswith(prefix) or item[1].startswith(prefix) or item[2].startswith(prefix)


def _compare(order_by, order='ascending'):
    def _cmp(a, b):
        if order_by == 'astno':
            return cmp(int(a['astno']), int(b['astno']))
        if order_by == 'astname':
            return cmp(a['astname'], b['astname'])
        if order_by == 'astdsg':
            return cmp(a['astdsg'], b['astdsg'])
        return cmp(a, b)

    def _ascending(a, b):
        return _cmp(a, b)

    def _descending(a, b):
        return -1 * _cmp(a, b)

    if order == 'ascending':
        return _ascending
    if order == 'descending':
        return _descending
    return _ascending


_spectrum = '0.97866232010304 0.98068712340672 0.98334827546704 0.98682313217344 0.99099352617552 0.99553124291776 1. 1.00230104684992 1.0032928997158401 1.00351742716068 1.00335463747072 1.00306566762124 1.0025622148526798 1.00177621204928 1.0005252693237598 0.99887759656 0.9965433154159201 0.99307434448 0.9887949021141199 0.9844778727493999 0.9795516450271999 0.9737885559134 0.9658749356999999 0.9545901137208 0.94021113058908 0.92519931979072 0.91196238936896 0.97866232010304 0.98068712340672 0.98334827546704 0.98682313217344 0.99099352617552 0.99553124291776 1. 1.00230104684992 1.0032928997158401 1.00351742716068 1.00335463747072 1.00306566762124 1.0025622148526798 1.00177621204928 1.0005252693237598 0.99887759656 0.9965433154159201 0.99307434448 0.9887949021141199 0.9844778727493999 0.9795516450271999\n'


def request_data(astno, astname, astdsg):
    if not USE_MOCK_DATA:
        logger.warning('Attempt to request non-mock data but none is available')
        raise NotImplementedError('Only mock data available')
    return _spectrum





def search(query, order_by, order, page_no, page_size):
    """
    Search mock database for asteroids
    
    Query database by a general prefix & ordering and paging spec
    Returns a page of results, if any, along with page number and total page count 
    """

    if not USE_MOCK_DATA:
        logger.warning('Attempt to request non-mock data but none is available')
        raise NotImplementedError('Only mock data available')

    prefix = ''.join(query.split()).lower() # normalize query string
    #page, pages_total = [], 0
    queryset = []

    if prefix:
        queryset = [{
            'astno': item[0],
            'astname': item[1].title(),
            'astdsg': item[2].upper(),
        } for item in _MOCK_SEARCH_RESULTS if _matches(prefix, item)]
        queryset.sort(_compare(order_by, order=order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 0)
    page = queryset[page_no * page_size - page_size:page_no * page_size]

    

    return page, page_no, pages_total




logger.info('{} loaded'.format(__name__))



