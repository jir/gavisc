


"""
Various configurable settings for GAVISC
"""

from logging import getLogger
logger = getLogger(__name__)


from django.conf import settings

from avi.utilities import path
from sys import modules
from os.path import dirname


VERSION = '1.1.1'
FW_VERSION = '1.0.0'

### paths to file system

OUTPUT_PATH = settings.OUTPUT_PATH
AVI_ROOT = dirname(modules[__name__].__file__)

OWN_JOBS_PATH = settings.OUTPUT_PATH
SHARE_PATH = '/data/shared' # hardcoded, currently no GAVIP config variable available
METADATA_PATH = 'metadata.json'

SPECC_BIN = path(AVI_ROOT, 'bin/specc')
SPECOCF_BIN = path(AVI_ROOT, 'bin/specocf')


### data source

DATA_URL = "http://gaia.esac.esa.int/tap-server/tap"
USE_MOCK_DATA = True
MOCK_PATH = path(AVI_ROOT, 'mock')

SEARCH_PREFIX = ''
SEARCH_RESULT_ORDER_BY = 'astno'
SEARCH_RESULT_ORDER = 'ascending'
SEARCH_RESULT_PAGE_NO = 1






logger.info('{} loaded'.format(__name__))
