from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

from django.http import Http404

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.data_source'] = load_source('avi.data_source', '../avi/data_source.py')
modules['avi.results'] = load_source('avi.results', '../avi/results.py')

modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.views'] = load_source('avi.views', '../avi/views.py')

from avi.views import _resolve_results
from avi.views import _job_mapper
from avi.views import _job_cmp
from avi.views import _normalize
from avi.views import _shared_job_mapper

from avi.views import index
from avi.views import searchAsteroids
from avi.views import computationParameters
from avi.views import ownComputations
from avi.views import sharedComputations
from avi.views import results
from avi.views import viz2d
from avi.views import ListSearchResults
from avi.views import ListOrCreateJobs
from avi.views import RetrieveOrDestroyJob
from avi.views import RetrieveJobProgress
from avi.views import share
from avi.views import download
from avi.views import reveal



from avi.models import Job
from avi.defaults import ALL_DEFAULTS
from avi.settings import VERSION


class TestViews(TestCase):
    def test_resolve_result(self):
        md5 = 'nosuchmd5'
        with self.assertRaises(Http404):
            _resolve_results(md5)

    def test_normalize(self):
        unnormalized = ALL_DEFAULTS.copy()
        normalized = _normalize(unnormalized)
        self.assertTrue(unnormalized.keys().sort() == normalized.keys().sort())

    def test_job_cmp(self):
        data = [
            {'x': 1},
            {'x': 3},
            {'x': -1},
            {'x': 2},
        ]
        ascending = _job_cmp('x', 'ascending')
        descending = _job_cmp('x', 'descending')
        unknown = _job_cmp('x', 'nosuchorder')

        self.assertTrue(sorted(data, ascending) == [
            {'x': -1},
            {'x': 1},
            {'x': 2},
            {'x': 3},
        ])

        self.assertTrue(sorted(data, descending) == [
            {'x': 3},
            {'x': 2},
            {'x': 1},
            {'x': -1},
        ])

        self.assertTrue(sorted(data, unknown) == [
            {'x': -1},
            {'x': 1},
            {'x': 2},
            {'x': 3},
        ])

    def test_job_mapper(self):
        job = Job()
        job.md5 = 'JOB_MD5'
        job.astno = 'JOB_ASTNO'
        job.astname = 'JOB_ASTNAME'
        job.astdsg = 'JOB_ASTDSG'

        # mapper should return below dict
        self.assertTrue(_job_mapper(job) == {
            'md5': 'JOB_MD5',
            'timestamp': '%Y-%m-%d %H:%M', # testing a correct format spec is passed
            'astno': 'JOB_ASTNO',
            'astname': 'JOB_ASTNAME',
            'astdsg': 'JOB_ASTDSG',
            'progress': 50,
            'status': 'PIPELINE_STATE',
        })

    ''' # TODO
    def test_job_paging(self):
        self.assertTrue(False)
    # '''

    def test_shared_job_mapper(self):
        metadata = {
            'md5': "metadata['md5']",
            'timestamp': "metadata['timestamp']",
            'astno': "metadata['astno']",
            'astname': "metadata['astname']",
            'astdsg': "metadata['astdsg']",
            'specc': "metadata['specc']",
            'specocf': "metadata['scpecocf']",
            'nosuchkey': "metadata['acs']",
        }
        mapped = _shared_job_mapper(metadata)
        self.assertTrue(sorted(mapped.keys()) == sorted([
            'md5',
            'timestamp',
            'astno',
            'astname',
            'astdsg',
            'specc',
            'specocf',
        ]))

    ''' # TODO
    def test_shared_job_paging(self):
        self.assertTrue(False)
    # '''


    def test_index(self):
        req = mock3.Request()
        resp = index(req)
        self.assertTrue(resp[0][1] == 'avi/introduction.html')
        self.assertTrue(resp[1]['context'] == {'version': VERSION})

    def test_searchAsteroids(self):
        req = mock3.Request()
        resp = searchAsteroids(req)
        self.assertTrue(resp[0][1] == 'avi/search-asteroids.html')

    def test_computationParameters(self):
        req = mock3.Request()
        resp = computationParameters(req)
        self.assertTrue(resp[0][1] == 'avi/computation-parameters.html')

    def test_ownComputations(self):
        req = mock3.Request()
        resp = ownComputations(req)
        self.assertTrue(resp[0][1] == 'avi/own-computations.html')

    def test_sharedComputations(self):
        req = mock3.Request()
        resp = sharedComputations(req)
        self.assertTrue(resp[0][1] == 'avi/shared-computations.html')

    def test_results(self):
        req = mock3.Request()
        resp = results(req)
        self.assertTrue(resp[0][1] == 'avi/results.html')
        self.assertTrue(type(resp[1]['context']) == dict)

    def test_viz2d(request, md5):
        req = mock3.Request()
        resp = viz2d(req)
        self.assertTrue(resp[0][1] == 'avi/.html')
        self.assertTrue(resp[1]['context'] == None)


    ''' # TODO
    def test_list_search_results(self):
        list_search_results = ListSearchResults()
        req = mock3.Request()
        resp = list_search_results.get(req)
        self.assertTrue(resp[0][1] == 'avi/.html')

    def test_list_or_create_jobs(self):
        list_or_create_jobs = ListOrCreateJobs()
        req = mock3.Request()
        resp = list_or_create_jobs.get(req)
        self.assertTrue(resp[0][1] == 'avi/.html')
        req = mock3.Request()
        resp = list_or_create_jobs.post(req)
        self.assertTrue(resp[0][1] == 'avi/.html')

    def test_retrieve_or_destroy_job(self):
        retrieve_or_destroy_jobs = RetrieveOrDestroyJob()
        req = mock3.Request()
        resp = retrieve_or_destroy_jobs.get(req)
        self.assertTrue(resp[0][1] == 'avi/.html')
        req = mock3.Request()
        resp = retrieve_or_destroy_jobs.post(req)
        self.assertTrue(resp[0][1] == 'avi/.html')

    def test_retrieve_job_progress(self):
        retrieve_job_progress = RetrieveJobProgress()
        req = mock3.Request()
        resp = retrieve_job_progress.get(req)
        self.assertTrue(resp[0][1] == 'avi/.html')

    def test_share(self):
        req = mock3.Request() # request not accessed
        resp = download(req, 'examplemd5')
        self.assertTrue(resp.content == 'USERSPACE SHARE LINK: JOB_PATH/GAVISC results/gavisc/0') #!!
        self.assertTrue(resp.content_type == 'text/plain')

    def test_download(self):
        req = mock3.Request() # request not accessed
        resp = download(req, 'examplemd5')
        self.assertTrue(resp.content == 'USERSPACE DOWNLOAD LINK: JOB_PATH')
        self.assertTrue(resp.content_type == 'text/plain')

    def test_reveal(self):
        req = mock3.Request() # request not accessed
        resp = reveal(req, 'examplemd5')
        self.assertTrue(resp.content == 'USERSPACE VIEW LINK: JOB_PATH')
        self.assertTrue(resp.content_type == 'text/plain')
    # '''

if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()


