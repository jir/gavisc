from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3 

from os.path import abspath

# import utilities.py into fixture
modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi', '../avi/utilities.py')

from avi.utilities import update_existing
from avi.utilities import path
from avi.utilities import suffix
from avi.utilities import load
from avi.utilities import save
from avi.utilities import ls
from avi.utilities import cmd
from avi.utilities import save_csv
from avi.utilities import save_vot
from avi.utilities import txt_to_csv
from avi.utilities import txt_to_vot



class TestUtilities(TestCase):

    def test_update_existing(self):
        # try updating empty dict
        a = {}
        update_existing(a, {
            'a': -1,
            'b': -2,
            'c': -3,
        })
        self.assertTrue(a == {})

        # disjoint update
        b = {
            'a': 1,
            'b': 2,
            'c': 3,
        }
        update_existing(b, {
            'd': 4,
            'e': 5,
            'f': 6,
        })
        self.assertTrue(b == {
            'a': 1,
            'b': 2,
            'c': 3,
        })

        # overlapping update
        c = {
            'a': 1,
            'b': 2,
            'c': 3,
        }
        update_existing(c, {
            'b': -2,
            'c': -3,
            'd': -4,
        })
        self.assertTrue(c == {
            'a': 1,
            'b': -2,
            'c': -3,
        })

    def test_path(self):
        self.assertTrue(path('') == '.')
        self.assertTrue(path('a', 'b', 'c') == 'a/b/c')
        self.assertTrue(path('a', 'b/', 'c') == 'a/b/c')
        self.assertTrue(path('a', 'b//', 'c') == 'a/b/c')
        self.assertTrue(path('a', 'b', 'c/') == 'a/b/c')
        self.assertTrue(path('a', 'b', 'c//') == 'a/b/c')
        self.assertTrue(path('/a', 'b', 'c') == '/a/b/c')
        self.assertTrue(path('a', '/b', 'c') == '/b/c')
        self.assertTrue(path('/a', '/b', 'c') == '/b/c')
        self.assertTrue(path(1, 2, 3) == '1/2/3')

        self.assertTrue(path('a', 'b', 'c', '.', 'd') == 'a/b/c/d')
        self.assertTrue(path('a', 'b', 'c', '/.', 'd') == '/d')
        self.assertTrue(path('a', 'b', 'c', './', 'd') == 'a/b/c/d')
        self.assertTrue(path('a', 'b', 'c', './/', 'd') == 'a/b/c/d')
        self.assertTrue(path('a', 'b', 'c', '..', 'd') == 'a/b/d')
        self.assertTrue(path('a', 'b', 'c', '../', 'd') == 'a/b/d')
        self.assertTrue(path('a', 'b', 'c', '..//', 'd') == 'a/b/d')
        self.assertTrue(path('a', 'b', 'c', '../.', 'd') == 'a/b/d')
        self.assertTrue(path('a', 'b', 'c', '.././', 'd') == 'a/b/d')

        self.assertTrue(path('a', 'b', 'c', '../../..') == '.')
        self.assertTrue(path('a', 'b', 'c', '../../../..') == '..')
        self.assertTrue(path('/a', 'b', 'c', '../../../..') == '/')

        with self.assertRaises(TypeError):
            path()

    def test_suffix(self):
        self.assertTrue(suffix('') == '')
        self.assertTrue(suffix('a') == '')
        self.assertTrue(suffix('.a') == '')
        self.assertTrue(suffix('a.') == '.')
        self.assertTrue(suffix('.a.') == '.')
        self.assertTrue(suffix('a.b') == '.b')
        self.assertTrue(suffix('a.b.') == '.')
        self.assertTrue(suffix('a/b.c') == '.c')
        self.assertTrue(suffix('a/b/c') == '')

        with self.assertRaises(TypeError):
            suffix()

    def test_load(self):
        fpath = abspath(__file__)
        with open(fpath) as f:
            self.assertTrue(f.read() == load(fpath))

        with self.assertRaises(IOError):
            data = load('/tmp/nosuchfile')

        with self.assertRaises(IOError):
            data = load('/')


    def test_save(self): pass
    def test_ls(self): pass

    def test_cmd(self):
        self.assertTrue(cmd('/usr/bin/stat') == None)
        self.assertTrue(cmd('/usr/bin/stat', '/usr/bin/stat') == None)
        with self.assertRaises(Exception):
            cmd('/usr/bin/stat', '/tmp/nosuchfile')
        with self.assertRaises(Exception):
            cmd('/') # can't execute root

    def test_save_csv(self): pass
    def test_save_vot(self): pass
    def test_txt_to_csv(self): pass
    def test_txt_to_vot(self): pass
    def test_create_tgz(self): pass


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()
