
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')

from avi.models import Job
from avi.utilities import path


class TestModels(TestCase):

    def test_Job(self):
        job = Job()
        self.assertTrue(hasattr(job, 'pipeline_task'))
        self.assertTrue(hasattr(job, 'md5'))
        self.assertTrue(hasattr(job, 'astno'))
        self.assertTrue(hasattr(job, 'astdsg'))
        self.assertTrue(hasattr(job, 'astname'))
        self.assertTrue(hasattr(job, 'csv'))
        self.assertTrue(hasattr(job, 'vot'))

    def test_Job_get_viz2d(self):
        job = Job()
        self.assertTrue(job.get_viz2d() == b'No plot available.')


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()



