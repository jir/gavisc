
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3


modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')


from avi.settings import VERSION
from avi.settings import FW_VERSION
from avi.settings import OUTPUT_PATH
from avi.settings import OWN_JOBS_PATH
from avi.settings import SHARE_PATH
from avi.settings import METADATA_PATH
from avi.settings import DATA_URL
from avi.settings import SPECC_BIN
from avi.settings import SPECOCF_BIN
from avi.settings import SEARCH_PREFIX
from avi.settings import SEARCH_RESULT_ORDER_BY
from avi.settings import SEARCH_RESULT_ORDER
from avi.settings import SEARCH_RESULT_PAGE_NO

from django.conf import settings

class TestSettings(TestCase):

    def test_version(self):
        self.assertTrue(VERSION == '1.1.1')
    def test_fw_version(self):
        self.assertTrue(FW_VERSION == '1.0.0')

    def test_output_path(self):
        self.assertTrue(OUTPUT_PATH == settings.OUTPUT_PATH)
    def test_own_jobs_path(self):
        self.assertTrue(OWN_JOBS_PATH == settings.OUTPUT_PATH)
    def test_share_path(self):
        self.assertTrue(SHARE_PATH == '/data/shared')
    def test_metadata_path(self):
        self.assertTrue(METADATA_PATH == 'metadata.json')

    def test_data_url(self):
        self.assertTrue(DATA_URL == 'http://gaia.esac.esa.int/tap-server/tap')

    def test_executables(self):
        self.assertTrue(SPECC_BIN.endswith('/bin/specc'))
        self.assertTrue(SPECOCF_BIN.endswith('/bin/specocf'))

    def test_search_defaults(self):
        self.assertTrue(SEARCH_PREFIX == '')
        self.assertTrue(SEARCH_RESULT_ORDER_BY == 'astno')
        self.assertTrue(SEARCH_RESULT_ORDER == 'ascending')
        self.assertTrue(SEARCH_RESULT_PAGE_NO == 1)


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()



