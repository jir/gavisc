
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')

from avi.defaults import ALL_DEFAULTS
from avi.defaults import JOB_STATES_COLLATION
from avi.defaults import DEFAULT_SEARCH_QUERY
from avi.defaults import DEFAULT_SEARCH_ORDER_BY
from avi.defaults import DEFAULT_SEARCH_ORDER
from avi.defaults import DEFAULT_JOBS_QUERY
from avi.defaults import DEFAULT_JOBS_ORDER_BY
from avi.defaults import DEFAULT_JOBS_ORDER
from avi.defaults import DEFAULT_PAGE_NO
from avi.defaults import DEFAULT_PAGE_SIZE

class TestDefaults(TestCase):
    def test_all_defaults(self):
        keys = [
            'astdsg',
            'astname',
            'astno',
            'csv',
            'vot',
        ]
        for key in keys:
            self.assertTrue(key in ALL_DEFAULTS)

    def test_job_states_collation(self):
        self.assertTrue(JOB_STATES_COLLATION['PENDING'] < JOB_STATES_COLLATION['STARTED'])
        self.assertTrue(JOB_STATES_COLLATION['STARTED'] < JOB_STATES_COLLATION['SUCCESS'])
        self.assertTrue(JOB_STATES_COLLATION['SUCCESS'] < JOB_STATES_COLLATION['FAILURE'])

    def test_query_defaults(self):
        self.assertTrue(DEFAULT_SEARCH_QUERY == '')
        self.assertTrue(DEFAULT_SEARCH_ORDER_BY == 'astno')
        self.assertTrue(DEFAULT_SEARCH_ORDER == 'ascending')
        self.assertTrue(DEFAULT_JOBS_QUERY == 'own')
        self.assertTrue(DEFAULT_JOBS_ORDER_BY == 'status')
        self.assertTrue(DEFAULT_JOBS_ORDER == 'ascending')
        self.assertTrue(DEFAULT_PAGE_NO == 1)
        self.assertTrue(DEFAULT_PAGE_SIZE == 50)
        


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()

