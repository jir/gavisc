from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3 

from os.path import normpath
from os.path import join

# mock relevant constant values
AVI_PARAMETER = 'AVI_PARAMETER'
OUTPUT_PATH = 'OUTPUT_PATH'
AVI_ROOT = 'AVI_ROOT'
MOCK_PATH = 'MOCK_PATH'
AIS_BIN = 'AIS_BIN'
AES_BIN = 'AES_BIN'
ACS_BIN = 'ACS_BIN'
FAILURE = 'FAILURE'



# mock importing
#modules['luigi'] = luigi
#modules['luigi.event'] = luigi.event
#modules['pipeline'] = pipeline
#modules['pipeline.classes'] = pipeline.classes

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.data_source'] = load_source('avi.data_source', '../avi/data_source.py')
modules['avi.data_preprocessing'] = load_source('avi.data_preprocessing', '../avi/data_preprocessing.py')
modules['avi.visualization'] = load_source('avi.visualization', '../avi/visualization.py')
modules['avi.tasks'] = load_source('avi.tasks', '../avi/tasks.py')

from pipeline.classes import AviParameter

# import avi.tasks into fixture
from avi.tasks import Task
from avi.tasks import Compute
from avi.tasks import Plot
from avi.tasks import PlotHTMLFile
from avi.tasks import CSV
from avi.tasks import VOTable
from avi.tasks import SPECC
from avi.tasks import SPECOCF
from avi.tasks import SPECCTaxvecTxtFile
from avi.tasks import SPECCProbvecTxtFile
from avi.tasks import SPECCOrderTxtFile
from avi.tasks import SPECCLDACoordinatesTxtFile
from avi.tasks import SPECCGroupvecTxt
from avi.tasks import SPECOCFFittedRealPartTxtFile
from avi.tasks import SPECOCFFittedImaginarySpectraTxtFile
from avi.tasks import SPECOCFFittedSpectraTxtFile
from avi.tasks import SPECOCFRseTxtFile
from avi.tasks import SPECOCFWavelengthsTxtFile
from avi.tasks import SPECOCFBestCaseTxtFile
from avi.tasks import SPECOCFOriginalSpectraTxtFile
from avi.tasks import SPECOCFSplineCoefficientsTxtFile
from avi.tasks import SPECOCFAlbedoTxtFile
from avi.tasks import ComputeSPECC
from avi.tasks import ComputeSPECOCF
from avi.tasks import InputDataTxtFile
from avi.tasks import PreprocessInputData
from avi.tasks import RequestInputData
from avi.tasks import Initialize


class TestTask(TestCase):
    def test_interface(self):
        task = Task()
        self.assertTrue(type(task.md5) == AviParameter)
        self.assertTrue(callable(task._cwd))
        self.assertTrue(callable(task._do))
        self.assertTrue(callable(task._dependencies))
        self.assertTrue(callable(task.requires))
        self.assertTrue(callable(task.run))
        self.assertTrue(callable(task.output))

class TestCompute(TestCase):
    def test_dependencies(self):
        task = Compute()
        self.assertTrue(task._dependencies() == (SPECC, SPECOCF, Plot, CSV, VOTable))

class TestPlot(TestCase):
    def test_dependencies(self):
        task = Plot()
        self.assertTrue(task._dependencies() == (PlotHTMLFile))

class TestPlotHTMLFile(TestCase):
    def test_dependencies(self):
        task = PlotHTMLFile()
        self.assertTrue(task._dependencies() == (SPECC, SPECOCF))

class TestCSV(TestCase):
    def test_dependencies(self):
        task = CSV()
        self.assertTrue(task._dependencies() == (SPECC, SPECOCF))

class TestVOTable(TestCase):
    def test_dependencies(self):
        task = VOTable()
        self.assertTrue(task._dependencies() == (SPECC, SPECOCF))

## bundle tasks
class TestSPECC(TestCase):
    
    def test_dependencies(self):
        task = SPECC()
        self.assertTrue(task._dependencies() == (
            SPECCTaxvecTxtFile,
            SPECCProbvecTxtFile,
            SPECCOrderTxtFile,
            SPECCLDACoordinatesTxtFile,
            SPECCGroupvecTxt,
        ))

class TestSPECOCF(TestCase):
    
    def test_dependencies(self):
        task = SPECOCF()
        self.assertTrue(task._dependencies() == (
            SPECOCFFittedRealPartTxtFile,
            SPECOCFFittedImaginarySpectraTxtFile,
            SPECOCFFittedSpectraTxtFile,
            SPECOCFRseTxtFile,
            SPECOCFWavelengthsTxtFile,
            SPECOCFBestCaseTxtFile,
            SPECOCFOriginalSpectraTxtFile,
            SPECOCFSplineCoefficientsTxtFile,
            SPECOCFAlbedoTxtFile,
        ))

## SPEC-C data product wrappers
class TestSPECCTaxvecTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECCTaxvecTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECC))

class TestSPECCProbvecTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECCProbvecTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECC))

class TestSPECCOrderTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECCOrderTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECC))

class TestSPECCLDACoordinatesTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECCLDACoordinatesTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECC))

class TestSPECCGroupvecTxt(TestCase):
    def test_dependencies(self):
        task = SPECCGroupvecTxt()
        self.assertTrue(task._dependencies() == (ComputeSPECC))

## SPEC-OCF data product wrappers

class TestSPECOCFFittedRealPartTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFFittedRealPartTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFFittedImaginarySpectraTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFFittedImaginarySpectraTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFFittedSpectraTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFFittedSpectraTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFRseTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFRseTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFWavelengthsTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFWavelengthsTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFBestCaseTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFBestCaseTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFOriginalSpectraTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFOriginalSpectraTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFSplineCoefficientsTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFSplineCoefficientsTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

class TestSPECOCFAlbedoTxtFile(TestCase):
    def test_dependencies(self):
        task = SPECOCFAlbedoTxtFile()
        self.assertTrue(task._dependencies() == (ComputeSPECOCF))

## main algorithm tasks

class TestComputeSPECC(TestCase):
    def test_dependencies(self):
        task = ComputeSPECC()
        self.assertTrue(task._dependencies() == (InputDataTxtFile))

class TestComputeSPECOCF(TestCase):
    def test_dependencies(self):
        task = ComputeSPECOCF()
        self.assertTrue(task._dependencies() == (InputDataTxtFile, SPECCOrderTxtFile))

## initial steps
class TestInputDataTxtFile(TestCase):
    def test_dependencies(self):
        task = InputDataTxtFile()
        self.assertTrue(task._dependencies() == (RequestInputData))

class TestPreprocessInputData(TestCase):
    def test_dependencies(self):
        task = PreprocessInputData()
        self.assertTrue(task._dependencies() == (RequestInputData))

class TestRequestInputData(TestCase):
    def test_dependencies(self):
        task = RequestInputData()
        self.assertTrue(task._dependencies() == (Initialize))

class TestInitialize(TestCase):
    def test_interface(self):
        task = Initialize()
        self.assertTrue(callable(task._cwd))
        self.assertTrue(callable(task.run))
        self.assertTrue(callable(task.output))


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()




